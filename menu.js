const toggleMenu = () => {
  let menuBox = document.getElementById('navbar');    
  if(menuBox.style.display == "block") {
    menuBox.style.display = "none";
  }
  else {
    menuBox.style.display = "block";
  }
}